# MIDI2WAV

## Install
```
pip install -e .
```

## Dataset
* Download [lmd_full](https://colinraffel.com/projects/lmd/)
* lastfm, tagtraum, msd : https://salu133445.github.io/lakh-pianoroll-dataset/labels
* matched_ids.txt (msd <-> lmd id mapping), midi_info.json : https://salu133445.github.io/lakh-pianoroll-dataset/dataset


## Setup
`constants.py` 안에 있는 path를 업데이트한다.
`ORIG_MIDI_DIR` : 오리지널 Lakh MIDI full 데이터셋 디렉토리 위치
`PARSED_MIDI_DIR`: 파싱한 Lakh MIDI 디렉토리를 저장할 위치
`VST_PATH`: 사용하는 VST가 각각 저장된 위치를 업데이트 해주어야함. 맥북의 경우 `/Library/Audio/Plug-Ins/` 경로안에 VST 또는 VST3 폴더 안에 주로 위치해있음.

## Test
```
py -m pytest
```
