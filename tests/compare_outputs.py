import numpy as np
import librosa
import pretty_midi


daw_wav_file = "daw_output/addictive_keys__acoustic_piano__studio_grand_default.wav"
dawdreamer_wav_file = "dawdreamer_output/addictive_keys__acoustic_piano__studio_grand_default.wav"

dawdreamer_output, _ = librosa.load(dawdreamer_wav_file, sr=44100, mono=True)
daw_output, _ = librosa.load(daw_wav_file, sr=44100, mono=True)

print (daw_output.shape)
print (dawdreamer_output.shape)
duration = min(daw_output.shape[0], dawdreamer_output.shape[0])
daw_output= daw_output[:duration]
dawdreamer_output =dawdreamer_output[:duration]

print (np.sum(np.abs(daw_output - dawdreamer_output)))
print (np.testing.assert_allclose(dawdreamer_output, daw_output))
