import os
import numpy as np
import librosa
from pathlib import Path
import pretty_midi
import pytest

from midi2wav.renderer import render
from midi2wav.utils import load_audio
from midi2wav.config import *

test_config = {
    # "realstrat5" : {
    #     "use_editor" : True,
    #     "instruments" : {
    #         "distortion_guitar" : ["presets/realstrat5/distortion_guitar/realstrat5__distortion_guitar__default"],
    #         "electric_guitar_clean" : ["presets/realstrat5/electric_guitar_clean/realstrat5__electric_guitar_clean__default"]
    #     },
    #     "fx" : {
    #         "guitar_rig" : {
    #             "distortion_guitar" : [
    #                 "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__alternative_crunch",
    #                 "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__cheesy_80s_metal_lead",
    #                 "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__straightforward_metal",
    #                 # "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__driven_delay",
    #                 # "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__double_screamer",
    #                 # "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__horrible_punk_tone",
    #                 # "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__rockin_jump"
    #             ],
    #             "electric_guitar_clean" : [
    #                 "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__clean_break",
    #                 "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__funky_autowah",
    #                 # "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__greasy_lightning",
    #                 # "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__simple_guitar_tone",
    #                 # "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__use_your_dynamics",
    #             ]
    #         }
    #     }
    # },

    # "mt_powerdrumkit" : {
    #     "use_editor" : True,
    #     "instruments" : {
    #         "drum" : ["presets/mt_powerdrumkit/drum/mt_powerdrumkit__drum__default"]
    #     }
    # },
    # "addictive_drums" : {
    #     "use_editor" : True,
    #     "instruments" : {
    #         "drum" : [
    #         # "presets/addictive_drums/drum/addictive_drums__drum__ad2_dryroom",
    #                   "presets/addictive_drums/drum/addictive_drums__drum__fairfax_bundle_la_rock",
    #                   ]
    #     }
    # },
    # "iowa_flute" : {
    #     "use_editor" : False,
    #     "instruments" : {
    #         "flute": ["presets/iowa_flute/flute/iowa_flute__flute__default.fxp"]
    #     }
    # },
    "iowa_alto_sax" : {
        "use_editor" : False,
        "instruments" : {
            # "alto_sax" : ["presets/iowa_alto_sax/alto_sax/iowa_alto_sax__saxophone__default.fxp"],
            # "soprano_sax" : ["presets/iowa_alto_sax/soprano_sax/iowa_alto_sax__saxophone__default.fxp"],
            # "tenor_sax" : ["presets/iowa_alto_sax/tenor_sax/iowa_alto_sax__saxophone__default.fxp"],
            "baritone_sax" : ["presets/iowa_alto_sax/baritone_sax/iowa_alto_sax__saxophone__default.fxp"]
        }
    },
    # "dsk_saxophones" : {
    #     "use_editor" : True,
    #     "instruments" : {
    #         "soprano_sax" : ["presets/dsk_saxophones/soprano_sax/dsk_saxophones__soprano_sax__default"],
    #         # "alto_sax" : ["presets/dsk_saxophones/alto_sax/dsk_saxophones__alto_sax__default"],
    #         # "tenor_sax" : ["presets/dsk_saxophones/tenor_sax/dsk_saxophones__tenor_sax__default"],
    #         # "baritone_sax" : ["presets/dsk_saxophones/baritone_sax/dsk_saxophones__tenor_sax__default"]
    #     }
    # }
    # "bbc_symphony": {
    #     "use_editor": False,
    #     "instruments": {
    #         # "cello" : ["presets/bbc_symphony/cello/bbc_symphony__cello__cellis_long.fxp"],
    #         # "violin" : ["presets/bbc_symphony/violin/bbc_symphony_violin_violin1_long.fxp"],
    #         # "viola" : ["presets/bbc_symphony/viola/bbc_symphony_viola_violoas_long.fxp"],
    #         # "flute": ["presets/bbc_symphony/flute/bbc_symphony__flute__long.fxp"],
    #         # "clarinet" : ["presets/bbc_symphony/clarinet/bbc_symphony__clarinet__long.fxp"].,
    #         # "piccolo" : ["presets/bbc_symphony/piccolo/bbc_symphony__piccolo__long.fxp"],
    #         "tuba" : ["presets/bbc_symphony/tuba/bbc_symphony__tuba__long.fxp"],
    #         # "trumpet": ["presets/bbc_symphony/trumpet/bbc_symphony__trumpet__long.fxp"],
    #         # "trombone" : ["presets/bbc_symphony/trombone/bbc_symphony__trombone__tenor_long.fxp"],
    #         # "french_horn": [
    #         #     "presets/bbc_symphony/french_horn/bbc_symphony__french_horn__long.fxp"
    #         # ],
    #             # "oboe" : ["presets/bbc_symphony/oboe/bbc_symphony__oboe__long.fxp"]
    #     },
    # },
    # "ample_guitar" : {
    #     "use_editor" : False,
    #     "instruments" : {
    #         "acoustic_guitar_steel" : ["presets/ample_guitar/acoustic_guitar_steel/ample_guitar__acoustic_guitar_steel__default.fxp"],
    #     }
    # },
    # "ample_bass" : {
    #     "use_editor" : False,
    #     "instruments" : {
    #         "electric_bass" : [
    #             "presets/ample_bass/electric_bass/ample_bass__electric_bass__default.fxp"
    #         ]
    #     }
    # },
    # "echosound_guitar": {
    #     "use_editor" : False,
    #     "instruments" : {
    #         "acoustic_guitar_nylon" : ["presets/echosound_guitar/acoustic_guitar_nylon/echosound_guitar__acoustic_guitar_nylon__default.vstpreset"],
    #         # "acoustic_guitar_steel" : ["presets/echosound_guitar/acoustic_guitar_steel/echosound_guitar__acoustic_guitar_steel__default.vstpreset"]
    #     }
    # },
    # "collab3": {
    #     "use_editor" : False,
    #     "instruments" : {
    #         "organ" : ["presets/collab3/organ/collab3__organ__default.fxp"]
    #     }
    # },
    # "labs" : {
    #     "use_editor" : False,
    #     "instruments" : {
    #         # "drum" : ["presets/labs/drum/labs__drum__drums.fxp"],
    #         # "bass" : ["presets/labs/bass/labs__bass__warm_bass_amp.fxp", "presets/labs/bass/labs__bass__classic_bass_amp.fxp"] ,
    #         # "cello": "",
    #         # "strings" : ["presets/labs/strings/labs__strings__strings_ensemble.fxp"],
    #         # "electric_bass" : ["presets/labs/electric_bass/labs__electric_bass__classic_bass_amp.fxp"],
    #         "organ" : ["presets/labs/organ/labs__organ__pipe_organ_full_organ.fxp"],
    #         # "electric_piano" : [
    #         #     "presets/labs/electric_piano/labs__electric_piano__electric_piano_di.fxp",
    #         #     "presets/labs/electric_piano/labs__electric_piano__soft_piano_soft_piano.fxp",
    #         #     # "presets/labs/electric_piano/labs__electric_piano__rare_groove_piano_short.fxp",
    #         #     # "presets/labs/electric_piano/labs__electric_piano__wurli_di.fxp",
    #         # ],
    #         # "choir" : ["presets/labs/choir/labs__choir__michahs_choir_the_choir.fxp"]
    #     }
    # },
    # "addictive_keys": {
    #     "use_editor" : True,
    #     "instruments" : {
    #         "acoustic_piano" : [
    #                             "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_studio_grand",
    #                             "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_mono_tube",
    #                             # "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_mr_bright",
    #                             # "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_roomy_pop",
    #                             "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_aged_strings"
    #                             ],
    #     }
    # },
    # "kontakt" : {
    #     "use_editor" : True,
    #     "instruments" : {
    #           # "flute" : ["presets/kontakt/flute/kontakt__flute__C_Flute1_Compact"],
    #           # "clarinet": ["presets/kontakt/clarinet/kontakt__clarinet__Bb_Clarinet_Compact"],
    #           # "piccolo" : ["presets/kontakt/piccolo/kontakt__piccolo__Flute_Piccolo_Compact"],
    #           # "english_horn" : ["presets/kontakt/english_horn/kontakt__english_horn__English_Horn_Compact"],
    #          # "oboe" : ["presets/kontakt/oboe/kontakt__oboe__Oboe1_compact"],
    #         # "strings" : ["presets/kontakt/strings/kontakt__strings__EnsString_All_Mix_Full_High"],
    #         # "cello" : ["presets/kontakt/cello/kontakt__cello__VLC2_Modern_Cello"],
    #         # "violin" : ["presets/kontakt/violin/kontakt__violin__VLN1_Solo_Violin"],
    #         # "viola" : ["presets/kontakt/viola/kontakt__viola__VLA1_Solo_Viola"],
    #          # "trumpet" : ["presets/kontakt/trumpet/kontakt__trumpet__Trumpet_1_Compact"],
    #          # "trombone" : ["presets/kontakt/trombone/kontakt__trombone__Trombone_1_Compact"],
    #          # "french_horn" : ["presets/kontakt/french_horn/kontakt__french_horn__French_horn_1_Compact"],
    #          # "electric_bass": ["presets/kontakt/electric_bass/kontakt__electric_bass__scarbee_jay_bass_both"],
    #             "slap_bass" : ["presets/kontakt/slap_bass/kontakt__slap_bass__scarbee_jay_bass_slap_both"],
    #         # "acoustic_guitar_steel" : ["presets/kontakt/acoustic_guitar_steel/kontakt__acoustic_guitar_steel__1Steel_Full_M_DE"],
    #         # "acoustic_guitar_nylon" : ["presets/kontakt/acoustic_guitar_nylon/kontakt__acoustic_guitar_nylon__1Nylon_Full_DE"],
    # #         "electric_guitar_clean": ["presets/kontakt/electric_guitar_clean/kontakt__electric_guitar_clean__1EG_Clean_Full_M_DE",
    # #                                     "presets/kontakt/electric_guitar_clean/kontakt__electric_guitar_clean__1EG_Clean_Bridge_Full_M_DE"
    # #          ],
    # #         # "electric_guitar_jazz" : "presets/kontakt/electric_guitar_jazz/kontakt__electric_guitar_jazz__chrishein_1",
    #         # "brass" : [
    #             # "presets/kontakt/brass/kontakt__brass__French_Horn_Ensemble_Compact",
    #             # "presets/kontakt/brass/kontakt__brass__Trombone_Ensemble_Compact",
    #             # "presets/kontakt/brass/kontakt__brass__Trumpet_Ensemble_Compact"
    #         # ]
    #     }
    # }
}

test_list = []

for vst, vst_config in test_config.items():
    vst_path = VST_PATH[vst]
    if vst == "realstrat5":
        for inst, vst_presets in vst_config["instruments"].items():
            for vst_preset in vst_presets:
                inst_vst_preset = vst_preset
                for fx_vst, fx_vst_presets_list in vst_config["fx"].items():
                    fx_vst_path = VST_PATH[fx_vst]
                    for fx_vst_preset in fx_vst_presets_list[inst]:
                        midi_files = list(
                            Path(f"tests/midi_input/test_{inst}").glob("*.mid")
                        )
                        for midi_file in midi_files:
                            daw_wav_file = f"{Path(inst_vst_preset).stem}___{Path(fx_vst_preset).stem}___{midi_file.stem}.wav"
                            dawdreamer_wav_file = f"{Path(inst_vst_preset).stem}___{Path(fx_vst_preset).stem}___{midi_file.stem}.wav"
                            midi_file = midi_file.as_posix()
                            test_list.append(
                                (
                                    vst_path,
                                    inst_vst_preset,
                                    fx_vst_path,
                                    fx_vst_preset,
                                    midi_file,
                                    daw_wav_file,
                                    dawdreamer_wav_file,
                                    vst_config["use_editor"],
                                )
                            )
    else:
        for inst, vst_presets in vst_config["instruments"].items():
            for vst_preset in vst_presets:
                midi_files = list(Path(f"tests/midi_input/test_{inst}").glob("*.mid"))
                for midi_file in midi_files:
                    daw_wav_file = f"{Path(vst_preset).stem}___{midi_file.stem}.wav"
                    dawdreamer_wav_file = (
                        f"{Path(vst_preset).stem}___{midi_file.stem}.wav"
                    )
                    midi_file = midi_file.as_posix()
                    test_list.append(
                        (
                            vst_path,
                            vst_preset,
                            None,
                            None,
                            midi_file,
                            daw_wav_file,
                            dawdreamer_wav_file,
                            vst_config["use_editor"],
                        )
                    )


class TestVSTs:
    # @pytest.mark.parametrize(
    #     "vst, vst_preset, midi_file, daw_wav_file, dawdreamer_wav_file, use_editor",
    #     test_single_vst,
    # )
    # def test_vst(
    #     self, vst, vst_preset, midi_file, daw_wav_file, dawdreamer_wav_file, use_editor
    # ):
    #
    #     dawdreamer_wav_file = os.path.abspath(
    #         f"tests/dawdreamer_output/{dawdreamer_wav_file}"
    #     )
    #     daw_wav_file = os.path.abspath(f"tests/daw_output/{daw_wav_file}")
    #
    #     # midi_file = os.path.abspath(f"tests/midi_input/{midi_file}")
    #
    #     mid = pretty_midi.PrettyMIDI(midi_file)
    #     duration = mid.get_end_time()
    #
    #     out = render(
    #         midi_file, dawdreamer_wav_file, duration, vst, vst_preset, use_editor
    #     )
    #
    #     # Returns 1 if there is audio rendered
    #     assert out == 1
    #
    #     # dawdreamer_output = load_audio(dawdreamer_wav_file, target_sr=44100, mono=True)
    #     # daw_output = load_audio(daw_wav_file, target_sr=44100, mono=True)
    #
    #     # duration = min(daw_output.shape[0], dawdreamer_output.shape[0])
    #     # daw_output= daw_output[:duration]
    #     # dawdreamer_output =dawdreamer_output[:duration]
    #
    #     # np.testing.assert_allclose(dawdreamer_output, daw_output, rtol=1e-2)

    @pytest.mark.parametrize(
        "inst_vst, inst_vst_preset, fx_vst, fx_vst_preset, midi_file, daw_wav_file, dawdreamer_wav_file, use_editor",
        test_list,
    )
    def test_vst(
        self,
        inst_vst,
        inst_vst_preset,
        fx_vst,
        fx_vst_preset,
        midi_file,
        daw_wav_file,
        dawdreamer_wav_file,
        use_editor,
    ):

        dawdreamer_wav_file = os.path.abspath(
            f"tests/dawdreamer_output/{dawdreamer_wav_file}"
        )
        daw_wav_file = os.path.abspath(f"tests/daw_output/{daw_wav_file}")

        # midi_file = os.path.abspath(f"tests/midi_input/{midi_file}")

        mid = pretty_midi.PrettyMIDI(midi_file)
        duration = mid.get_end_time()

        out = render(
            midi_file,
            dawdreamer_wav_file,
            duration,
            inst_vst,
            inst_vst_preset,
            fx_vst,
            fx_vst_preset,
            use_editor,
        )
        # Returns 1 if there is audio rendered
        assert out == 1

        # dawdreamer_output = load_audio(dawdreamer_wav_file, target_sr=44100, mono=True)
        # daw_output = load_audio(daw_wav_file, target_sr=44100, mono=True)

        # duration = min(daw_output.shape[0], dawdreamer_output.shape[0])
        # daw_output= daw_output[:duration]
        # dawdreamer_output =dawdreamer_output[:duration]

        # np.testing.assert_allclose(dawdreamer_output, daw_output, rtol=1e-2)
