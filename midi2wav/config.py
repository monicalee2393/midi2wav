######################## Update ##########################
ORIG_MIDI_DIR = "C:/Users/a/Downloads/lmd_full"
PARSED_MIDI_DIR = "./lmd_parsed"
##########################################################
PRESET_DIR = "./presets"



SAMPLE_RATE = 44100
BUFFER_SIZE = 128

INSTRUMENT_NAMES = [
    "piano",
    "drum",
    "bass",
    "electric_guitar",
    "acoustic_guitar",
    "strings",
    "brass",
    "organ",
    "choir",
    "woodwinds",
    "saxophone",
    "others",
]

INSTRUMENT_NAME_TO_MIDI_PROGRAM_NAME = {
    "piano": [
        "Acoustic Grand Piano",
        "Bright Acoustic Piano",
        "Electric Grand Piano",
        "Honky-tonk Piano",
        "Electric Piano 1",
        "Electric Piano 2",
        "Harpsichord",
        "Clavinet",
    ],
    "acoustic_guitar": [
        "Acoustic Guitar (nylon)",
        "Acoustic Guitar (steel)",
        "Guitar Fret Noise",
    ],
    "electric_guitar": [
        "Electric Guitar (jazz)",
        "Electric Guitar (clean)",
        "Electric Guitar (muted)",
        "Overdriven Guitar",
        "Distortion Guitar",
        "Guitar Harmonics",
    ],
    "bass": [
        "Acoustic Bass",
        "Electric Bass (finger)",
        "Electric Bass (pick)",
        "Fretless Bass",
        "Slap Bass 1",
        "Slap Bass 2",
        "Synth Bass 1",
        "Synth Bass 2",
        "Contrabass",
    ],
    "drum": [
        "Drum",
        "Drums",
        "Timpani",
        "Tinkle Bell",
        "Agogo",
        "Steel Drums",
        "Woodblock",
        "Taiko Drum",
        "Melodic Tom",
        "Synth Drum",
        "Reverse Cymbal",
        "Timpani",
    ],
    "organ": [
        "Drawbar Organ",
        "Percussive Organ",
        "Rock Organ",
        "Church Organ",
        "Reed Organ",
    ],
    "strings": [
        "Violin",
        "Viola",
        "Cello",
        "Tremolo Strings",
        "Pizzicato Strings",
        "Orchestral Harp",
        "String Ensemble 1",
        "String Ensemble 2",
        "Synth Strings 1",
        "Synth Strings 2",
    ],
    "brass": [
        "Trumpet",
        "Trombone",
        "Tuba",
        "Muted Trumpet",
        "French Horn",
        "Brass Section",
        "Synth Brass 1",
        "Synth Brass 2",
    ],
    "choir": ["Choir Aahs", "Voice Oohs", "Synth Choir", "Breath Noise"],
    "woodwinds": [
        "Oboe",
        "English Horn",
        "Bassoon",
        "Clarinet",
        "Piccolo",
        "Flute",
        "Recorder",
        "Pan Flute",
        "Blown bottle",
        "Shakuhachi",
        "Whistle",
        "Ocarina",
    ],
    "saxophone": ["Soprano Sax", "Alto Sax", "Tenor Sax", "Baritone Sax"],
}

MIDI_PROGRAM_NAME_TO_INSTRUMENT_NAME = {
    v: k for k, vs in INSTRUMENT_NAME_TO_MIDI_PROGRAM_NAME.items() for v in vs
}

VST_TO_MIDI_INST = {
    "acoustic_piano": ["Acoustic Grand Piano", "Bright Acoustic Piano"],
    "electric_piano": [
        "Electric Grand Piano",
        "Honky-tonk Piano",
        "Electric Piano 1",
        "Electric Piano 2",
        "Clavinet",
        "Harpsichord",
    ],
    "acoustic_guitar_nylon": ["Acoustic Guitar (nylon)"],
    "acoustic_guitar_steel": ["Acoustic Guitar (steel)", "Guitar Fret Noise"],
    "electric_guitar_clean": [
        "Electric Guitar (clean)",
        "Electric Guitar (muted)",
        "Guitar Harmonics",
    ],
    "electric_guitar_jazz": ["Electric Guitar (jazz)"],
    # "overdriven_guitar": [
    #     "Overdriven Guitar",
    # ],
    "distortion_guitar": [
        "Overdriven Guitar",
        "Distortion Guitar",
    ],
    # "guitar_harmonics": [
    #     "Guitar Harmonics"
    # ],

    "organ": [
        "Drawbar Organ",
        "Percussive Organ",
        "Rock Organ",
        "Church Organ",
        "Reed Organ",
    ],
    "electric_bass": [
        "Acoustic Bass",
        "Electric Bass (finger)",
        "Electric Bass (pick)",
        "Fretless Bass"
    ],
    "synth_bass" :[
        "Synth Bass 1",
        "Synth Bass 2",
    ],
    "slap_bass": [
        "Slap Bass 1",
        "Slap Bass 2"
    ],
    "drum": [
        "Drum",
        "Tinkle Bell",
        "Agogo",
        "Steel Drums",
        "Woodblock",
        "Taiko Drum",
        "Melodic Tom",
        "Synth Drum",
        "Reverse Cymbal",
        "Timpani",
    ],
    "violin": ["Violin"],
    "viola": ["Viola"],
    "cello": ["Cello"],
    "contrabass": ["Contrabass"],
    "strings": [
        "Tremolo Strings",
        "Pizzicato Strings",
        "String Ensemble 1",
        "String Ensemble 2",
        "Synth Strings 1",
        "Synth Strings 2",
    ],
    "harp": ["Orchestral Harp"],
    # "timpani": ["Timpani"],
    "choir": ["Choir Aahs", "Voice Oohs", "Synth Choir", "Breath Noise"],
    "trumpet": ["Trumpet", "Muted Trumpet"],
    "trombone": ["Trombone"],
    "tuba": ["Tuba"],
    "french_horn": ["French Horn"],
    "english_horn": ["English Horn"],
    "brass": ["Brass Section", "Synth Brass 1", "Synth Brass 2"],
    "soprano_sax": ["Soprano Sax"],
    "alto_sax": ["Alto Sax"],
    "tenor_sax": ["Tenor Sax"],
    "baritone_sax": ["Baritone Sax"],
    "oboe": ["Oboe"],
    "bassoon": ["Bassoon"],
    "clarinet": ["Clarinet"],
    "piccolo": ["Piccolo"],
    "flute": [
        "Flute",
        "Recorder",
        "Pan Flute",
        "Blown bottle",
        "Shakuhachi",
        "Whistle",
        "Ocarina",
    ],
}


MIDI_INST_TO_VST = {v: k for k, vs in VST_TO_MIDI_INST.items() for v in vs}

MSG_INST_TO_VST = {
    "piano" : ["acoustic_piano", "electric_piano"],
    "drum" : ["drum"],
    "bass" : ["electric_bass", "slap_bass", "synth_bass", "contrabass"],
    "electric_guitar": ["electric_guitar_clean", "distortion_guitar", "electric_guitar_jazz"],
    "acoustic_guitar": ["acoustic_guitar_steel", "acoustic_guitar_nylon"],
    "strings": ["violin", "viola", "cello", "strings", "harp"],
    "brass" : ["trumpet", "trombone", "tuba", "french_horn", "brass"],
    "organ": ["organ"],
    "choir" : ["choir"],
    "woodwinds": ["flute", "clarinet", "oboe", "english_horn", "piccolo", "bassoon"],
    "saxophone": ["soprano_sax", "alto_sax", "tenor_sax", "baritone_sax"],

}

VST_TO_MSG_INST = {v: k for k, vs in MSG_INST_TO_VST.items() for v in vs}


# VST path
VST_PATH = {
    "kontakt": "C:/Program Files/Native Instruments/VSTPlugins 64 bit/Kontakt.dll",
    "labs": "C:/Program Files/Steinberg/VSTPlugins/LABS (64 Bit).dll",
    "mt_powerdrumkit": "C:/Program Files/Common Files/VST2/MT-PowerDrumKit.dll",
    "ample_bass": "C:/Program Files/Ample Sound/ABPL2.dll",
    "ample_guitar": "C:/Program Files/Ample Sound/AGML2.dll",
    "realstrat5": "C:/Program Files/Common Files/MusicLab/VST2/RealStrat.dll",
    "guitar_rig": "C:/Program Files/Native Instruments/VSTPlugins 64 bit/Guitar Rig 6.dll",
    "echosound_guitar": "C:/Program Files/Common Files/VST3/VinylGuitar x64.vst3",
    "iowa_flute": "C:/Program Files/Iowa/Iowa Flute - 64.dll",
    "iowa_alto_sax": "C:/Program Files/Iowa/Iowa Alto Sax - 64.dll",
    "collab3": "C:/Program Files/Steinberg/VSTPlugins/CollaB3 x64.dll",
    "dsk_saxophones": "C:/Program Files/Common Files/VST2/DSK SaxophoneZ.dll",
    "bbc_symphony": "C:/Program Files/Steinberg/VSTPlugins/BBC Symphony Orchestra (64 Bit).dll",
    "addictive_keys": "C:/Program Files/Steinberg/VSTPlugins/Addictive Keys.dll",
    "addictive_drums": "C:/Program Files/Steinberg/VSTPlugins/Addictive Drums 2.dll",
    "tal_bassline" : "C:/Program Files/Common Files/VST2/TAL-BassLine.dll"
    # "layers": "",
    # "dexed": "",
}

VST_PATH_TO_NAME = {v: k for k, v in VST_PATH.items() }


MIDI_RANGE_MAP = {
    "kontakt" : {
        "kontakt__electric_bass__scarbee_jay_bass_both" : [35, 76, 1],
        "kontakt__acoustic_guitar_steel__1Steel_Full_M_DE": [47, 103, 0],
        "kontakt__electric_guitar_clean__1EG_Clean_Full_M_DE": [47, 103, 0],
        "kontakt__slap_bass__scarbee_jay_bass_slap_both": [47, 103, 1],
        "kontakt__acoustic_guitar_nylon__1Nylon_Full_DE": [47, 100, 1],
        "kontakt__acoustic_guitar_steel__1Steel_Full_M_DE": [47, 100, 1],
        "kontakt__contrabass__CB1_Solo_Contrabass": [33, 108, 1],
        "kontakt__contrabass__CB2_German_Bass": [33, 108, 1],
        "kontakt__contrabass__CB3_Italian_Bass": [33, 108, 1]
    },
    "ample_bass" : {
        "ample_bass__electric_bass__default.fxp": [40, 88, 1],
    },
    "labs" : {
        "labs__electric_bass__classic_bass_amp.fxp": [24, 64, 0],
        "labs__organ__pipe_organ_full_organ.fxp": [24, 84, -1],
    },
    "realstrat5" : {
        "realstrat5__distortion_guitar__default" : [40,  86, 0],
        "realstrat5__electric_guitar_clean__default" : [40,  86, 0],
    },
    "ample_guitar": {
        "ample_guitar__acoustic_guitar_steel__default.fxp": [40, 84, 0],
    },
    "echosound_guitar" : {
        "echosound_guitar__acoustic_guitar_steel__default.vstpreset": [36, 83, 0],
    },
    "iowa_alto_sax": {
        "" : [59, 93,  0]
    }
}



# VST list per instrument
INST_VST_LIST = {
    "drum": ["labs"],  # mt_powerdrumkit, addictive_drums
    "acoustic_piano": ["addictive_keys"],  # "truepianos",
    "electric_piano": ["labs"],
    "electric_bass": ["kontakt","labs", "ample_bass"],
    "organ": ["collab3", "labs"],
    "acoustic_guitar_nylon": ["echosound_guitar", "kontakt"],
    "acoustic_guitar_steel": ["echosound_guitar", "ample_guitar", "kontakt"],
    "electric_guitar_clean": ["kontakt", "labs", "realstrat5"],
    "electric_guitar_jazz": ["kontakt"],
    # "overdriven_guitar": [],
    "distortion_guitar": ["realstrat5"],
    # "guitar_harmonics" : [],
    "strings": ["labs", "kontakt"],
    "brass": ["kontakt"],
    "choir": ["labs"],
    "woodwinds": ["kontakt"],
    "saxophone": ["iowa_alto_sax"],
    "violin": ["bbc_symphony", "kontakt"],
    "viola": ["bbc_symphony", "kontakt"],
    "cello": ["bbc_symphony", "kontakt"],
    "contrabass": ["kontakt"],
    # "timpani": [],
    "trumpet": ["bbc_symphony", "kontakt"],
    "trombone": ["bbc_symphony", "kontakt"],
    "tuba": ["bbc_symphony"],
    "french_horn": ["bbc_symphony", "kontakt"],
    "english_horn": ["kontakt"],
    "flute": ["bbc_symphony", "kontakt", "iowa_flute"],
    "oboe": ["bbc_symphony", "kontakt"],
    "bassoon": ["bbc_symphony", "kontakt"],
    "clarinet": ["bbc_symphony", "kontakt"],
    "piccolo": ["bbc_symphony", "kontakt"],
    "harp": ["labs"],
}
