from pathlib import Path
from collections import defaultdict
import json
import csv
from constants import * 

preset_dir = "/Users/kyungyunlee/Dropbox (Gaudio)/a_ai/am_MSG/amv_VST_preset/Howard"

instrument_list = VST_TO_MIDI_INST.keys()

print (instrument_list)

vst_list = list(Path(preset_dir).glob("*"))
vst_preset_map = defaultdict()

for vst_dir in vst_list :
    print (vst_dir)

    vst = vst_dir.stem

    preset_list =  [] 
    curr_insts = list(vst_dir.glob(f"**")) 
    curr_insts = [inst.name for inst in curr_insts if inst.name != vst]
    print (curr_insts)

    vst_preset_map[vst] = {}

    for inst in curr_insts : 
        preset_list = [] 
        for ext in ["FXP", "fxp", "vstpreset"] :
            presets_ = list((Path(vst_dir) / inst).glob(f"*.{ext}"))
            preset_list.extend(presets_)
        
        preset_list = [preset.name for preset in preset_list] 
        
        vst_preset_map[vst][inst] = preset_list

print (vst_preset_map)


inst_vst_map = defaultdict()

for vst, inst_map in vst_preset_map.items() : 
    print (vst)
    
    for inst, preset_list in inst_map.items() : 
        print (inst, preset_list)
        if inst not in inst_vst_map.keys() :
            inst_vst_map[inst] = {}
        
        inst_vst_map[inst][vst] = preset_list
print (inst_vst_map)

with open("vst_preset_map.json", "w") as f :
    json.dump(inst_vst_map, f)

with open("vst_preset_map.csv", "w") as f :
    csvwriter = csv.DictWriter(f, fieldnames=["instrument", "vst", "preset"])
    csvwriter.writeheader()

    for inst, vst_map in inst_vst_map.items() :
        for vst, preset_list in vst_map.items() : 
            for preset in preset_list : 
                csvwriter.writerow({"instrument": inst, "vst": vst, "preset": preset})



