import csv 
import numpy as np 
from collections import defaultdict
from constants import *

genre_count = defaultdict(int)
per_inst_count = defaultdict(int)


inst_occurence_list = [] 


with open("lmd_filtered.csv", "r") as f:
    reader = csv.DictReader(f)
    for row in reader:
        genres = row["genres"].split(",")
        for genre in genres:
            genre_count[genre] +=1 

        occ_list = [] 
        for inst in INSTRUMENT_NAMES:
            per_inst_count[inst] += int(row[inst])
            occ_list.append(int(row[inst]))
        
        inst_occurence_list.append(occ_list)

inst_occurence_list = np.array(inst_occurence_list)

print (inst_occurence_list.shape)

co_matrix = np.dot(inst_occurence_list.T, inst_occurence_list)
print (co_matrix.shape)
print (co_matrix)

co_diagonal = np.diagonal(co_matrix)
print (co_diagonal)
# with np.errstate(divide='ignore', invalid='ignore'):
co_percentage = np.nan_to_num(np.true_divide(co_matrix, co_diagonal[:, None]))
print (co_percentage)
# print('\ncooccurrence_matrix_percentage:\n{0}'.format(cooccurrence_matrix_percentage))

# print(genre_count)
# print (per_inst_count)

# Tag co-occurence 
