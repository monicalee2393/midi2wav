import os
import dawdreamer as daw
import soundfile as sf
from pathlib import Path
import time
import numpy as np
import pydub
import pretty_midi
from config import *

THRESHOLD = 0.001
AMP_MIN = -0.5
AMP_MAX = 0.5


def rescale_linear(array, new_min, new_max):
    """Rescale an arrary linearly."""
    scaled_array = np.zeros_like(array)
    for i in range(2):
        minimum, maximum = np.min(array[i]), np.max(array[i])
        m = (new_max - new_min) / (maximum - minimum)
        b = new_min - m * minimum
        scaled_array[i] = m * array[i] + b

    return scaled_array


def modify_midi(midi_path, vsti, preset_path):
    """
    Delete notes beyond the instrument range. Transpose octaves depending on vst.
    """

    low, high, octave_shift = MIDI_RANGE_MAP[vsti][preset_path]

    tmp_midi_path = midi_path.replace(".mid", "@TMP.mid")

    midi_data = pretty_midi.PrettyMIDI(midi_path)
    instrument = midi_data.instruments[0]
    notes = []
    # Don't want to shift drum notes
    if not instrument.is_drum:
        for note in instrument.notes:

            note.pitch += octave_shift * 12

            if note.pitch < low :
                continue
            if note.pitch > high :
                continue

            notes.append(note)

    instrument.notes = notes
    midi_data.instruments = [instrument]

    midi_data.write(tmp_midi_path)

    return tmp_midi_path


def render_and_save_audio(engine, duration, output_wav_path):

    # 오디오 랜더링할때 잔향 등을 모델링 하기위해 더 길게 랜더링
    duration += 5

    engine.render(duration)  # Render n seconds audio.
    audio = engine.get_audio()  # Returns numpy.ndarray shaped (2, NUM_SAMPLES)

    audio = audio[:2]
    # Normalize audio
    normalized_audio = rescale_linear(audio, AMP_MIN, AMP_MAX)

    # Check if there actually is audio
    if np.mean(np.abs(normalized_audio)) < THRESHOLD:
        print(f"WARNING: MIDI not rendered properly...{output_wav_path}")
        return 0

    sf.write(output_wav_path, normalized_audio.transpose(), SAMPLE_RATE)
    print(f"Saved to {output_wav_path}")

    return 1


def render(
    midi_path,
    output_wav_path,
    duration,
    inst_vst,
    inst_vst_preset_path,
    fx_vst=None,
    fx_vst_preset_path=None,
    use_editor=False,
    force_editor=False,
):
    """
    Render single midi file with VSTi and/or FX VST

    Params :
        midi_path_list : absolute path to the midi file
        output_wav_path_list : absolute path to where the wav file will be saved
        duration_list : duration of the output wav file (in seconds)
        inst_vst : absolute path to the instrument VST
        inst_vst_preset_path :
        fx_vst : absolute path to the FX VST
        fx_vst_preset_path :
    Returns :
        None
    """

    engine = daw.RenderEngine(SAMPLE_RATE, BUFFER_SIZE)

    # Load instrument
    inst_synth = engine.make_plugin_processor("my_synth", inst_vst)
    assert inst_synth.get_name() == "my_synth"
    time.sleep(0.5)

    if use_editor:
        # assert synth.load_preset(vst_preset_path)
        if not os.path.exists(inst_vst_preset_path) or force_editor:
            os.makedirs(os.path.dirname(inst_vst_preset_path), exist_ok=True)
            inst_synth.open_editor()
            inst_synth.save_state(inst_vst_preset_path)
        else:
            inst_synth.load_state(inst_vst_preset_path)
        # assert synth.load_vst3_preset("C:/Users/a/Downloads/chris_hein_3eg.vstpreset")
    else:
        if inst_vst_preset_path:
            if ".vst3" in inst_vst:
                inst_synth.load_vst3_preset(inst_vst_preset_path)
            else:
                inst_synth.load_preset(inst_vst_preset_path)
    time.sleep(0.5)

    # Load FX
    if fx_vst :
        fx_synth = engine.make_plugin_processor("my_synth2", fx_vst)
        assert fx_synth.get_name() == "my_synth2"
        time.sleep(0.5)

        if use_editor:
            # assert synth.load_preset(vst_preset_path)
            if not os.path.exists(fx_vst_preset_path):
                os.makedirs(os.path.dirname(fx_vst_preset_path), exist_ok=True)
                fx_synth.open_editor()
                fx_synth.save_state(fx_vst_preset_path)
            else:
                fx_synth.load_state(fx_vst_preset_path)
            # assert synth.load_vst3_preset("C:/Users/a/Downloads/chris_hein_3eg.vstpreset")
        else:
            if fx_vst_preset_path:
                if ".vst3" in fx_vst:
                    fx_synth.load_vst3_preset(fx_vst_preset_path)
                else:
                    fx_synth.load_preset(fx_vst_preset_path)
        time.sleep(0.5)


    if not os.path.exists(os.path.dirname(output_wav_path)):
        os.makedirs(os.path.dirname(output_wav_path), exist_ok=True)

    # Temporarily modify MIDI file
    vst_name = VST_PATH_TO_NAME[inst_vst]
    vst_preset_stem = Path(inst_vst_preset_path).name

    if vst_name in MIDI_RANGE_MAP.keys() :
        if vst_preset_stem in MIDI_RANGE_MAP[vst_name].keys():
            tmp_midi_path = modify_midi(midi_path, vst_name, vst_preset_stem)
            assert inst_synth.load_midi(tmp_midi_path)
            # Delete tmp midi file
            os.remove(tmp_midi_path)
        else :
            assert inst_synth.load_midi(midi_path)

    else :
        assert inst_synth.load_midi(midi_path)

    time.sleep(0.5)

    print("num_midi_event", inst_synth.n_midi_events)

    if fx_vst :
        graph = [(inst_synth, []), (fx_synth, [inst_synth.get_name()])]
    else :
        graph = [
            (inst_synth, []),
        ]

    engine.load_graph(graph)

    return render_and_save_audio(engine, duration, output_wav_path)


def render_multiple(
    midi_path_list,
    output_wav_path_list,
    duration_list,
    inst_vst,
    inst_vst_preset_path,
    fx_vst=None,
    fx_vst_preset_path=None,
    use_editor=False,
    force_editor=False
):
    """
    Render multiple midi files with VSTi *AND* FX VST

    Params :
        midi_path_list : absolute path to the midi file
        output_wav_path_list : absolute path to where the wav file will be saved
        duration_list : duration of the output wav file (in seconds)
        inst_vst : absolute path to the instrument VST
        inst_vst_preset_path :
        fx_vst : absolute path to the FX VST
        fx_vst_preset_path :
    Returns :
        None
    """

    engine = daw.RenderEngine(SAMPLE_RATE, BUFFER_SIZE)

    # Load instrument
    inst_synth = engine.make_plugin_processor("my_synth", inst_vst)
    assert inst_synth.get_name() == "my_synth"
    time.sleep(0.5)

    if use_editor:
        # assert synth.load_preset(vst_preset_path)
        if not os.path.exists(inst_vst_preset_path) or force_editor:
            os.makedirs(os.path.dirname(inst_vst_preset_path), exist_ok=True)
            inst_synth.open_editor()
            inst_synth.save_state(inst_vst_preset_path)
        else:
            inst_synth.load_state(inst_vst_preset_path)
        # assert synth.load_vst3_preset("C:/Users/a/Downloads/chris_hein_3eg.vstpreset")
    else:
        if inst_vst_preset_path:
            if ".vst3" in inst_vst:
                inst_synth.load_vst3_preset(inst_vst_preset_path)
            else:
                inst_synth.load_preset(inst_vst_preset_path)
    time.sleep(0.5)

    # Load FX
    if fx_vst :
        fx_synth = engine.make_plugin_processor("my_synth2", fx_vst)
        assert fx_synth.get_name() == "my_synth2"
        time.sleep(0.5)

        if use_editor:
            # assert synth.load_preset(vst_preset_path)
            if not os.path.exists(fx_vst_preset_path):
                os.makedirs(os.path.dirname(fx_vst_preset_path), exist_ok=True)
                fx_synth.open_editor()
                fx_synth.save_state(fx_vst_preset_path)
            else:
                fx_synth.load_state(fx_vst_preset_path)
            # assert synth.load_vst3_preset("C:/Users/a/Downloads/chris_hein_3eg.vstpreset")
        else:
            if fx_vst_preset_path:
                if ".vst3" in fx_vst:
                    fx_synth.load_vst3_preset(fx_vst_preset_path)
                else:
                    fx_synth.load_preset(fx_vst_preset_path)
        time.sleep(0.5)

    # Go through midi files
    for midi_path, output_wav_path, duration in zip(
        midi_path_list, output_wav_path_list, duration_list
    ):
        if not os.path.exists(os.path.dirname(output_wav_path)):
            os.makedirs(os.path.dirname(output_wav_path), exist_ok=True)

        # Temporarily modify MIDI file
        vst_name = VST_PATH_TO_NAME[inst_vst]
        vst_preset_stem = Path(inst_vst_preset_path).name

        if vst_name in MIDI_RANGE_MAP.keys() :
            if vst_preset_stem in MIDI_RANGE_MAP[vst_name].keys():
                tmp_midi_path = modify_midi(midi_path, vst_name, vst_preset_stem)
                assert inst_synth.load_midi(tmp_midi_path)
                # Delete tmp midi file
                os.remove(tmp_midi_path)
            else :
                assert inst_synth.load_midi(midi_path)

        else :
             assert inst_synth.load_midi(midi_path)

        time.sleep(0.5)

        print("num_midi_event", inst_synth.n_midi_events)

        if fx_vst :
            graph = [(inst_synth, []), (fx_synth, [inst_synth.get_name()])]
        else :
            graph = [(inst_synth, [])]

        engine.load_graph(graph)

        render_and_save_audio(engine, duration, output_wav_path)


if __name__ == "__main__":
    midi_path = "C:/Users/a/dev/midi2wav/tests/midi_input/test_electric_bass/lakhmidi_bass.mid"
    output_wav_path = "./test0524.wav"
    duration = 30.0
    # vst = "C:/Program Files/Common Files/VST3/Kontakt.vst3"
    vst = "C:/Program Files/Native Instruments/VSTPlugins 64 bit/Kontakt.dll"
    vst_preset_path = "C:/Users/a/dev/midi2wav/presets/kontakt/electric_bass/kontakt__electric_bass__scarbee_jay_bass_both"
    # vst_preset_path = "./presets/test_kontakt"
    # vst = "C:/Program Files/Ample Sound/AGML2.dll"
    # vst = "C:/Program Files/Common Files/MusicLab/VST2/RealStrat.dll"
    # vst_preset_path = "C:/Users/a/dev/midi2wav/presets/realstrat5/electric_guitar_clean/electric_guitar_clean"

    # vst = "C:/Program Files/Common Files/VST3/VinylGuitar x64.vst3"
    # vst = "C:/Program Files/Iowa_flute/Iowa Flute - 64.dll"
    # vst = "C:/Program Files/Steinberg/VSTPlugins/LABS (64 Bit).dll"
    # vst = "C:/Program Files/Steinberg/VSTPlugins/Addictive Drums 2.dll"
    # vst_preset_path = f"C:/Users/a/dev/midi2wav/presets/addictive_drums/drum/addictive_drums_drum_black_velvet_mullet_bitf2"
    # vst = "C:/Program Files/Common Files/VST2/MT-PowerDrumKit.dll"
    # vst_preset_path =  "C:/Users/a/dev/midi2wav/presets/mt_powerdrumkit/drum/mt_powerdrumkit_drum_default.fxp"
    # vst ="C:/Program Files/Steinberg/VSTPlugins/Addictive Keys.dll"
    # vst_preset_path = "C:/Users/a/dev/midi2wav/presets/addictive_keys/acoustic_piano/addictive_keys_acoustic_piano_studio_grand_ambient.FXP"

    # vst = "C:/Program Files/DSK Saxophones - win64/DSK Saxophones.dll"
    # vst_preset_path = "presets/dsk_saxophones/saxophone/dsk_saxophones_saxophone_tenor_default.FXP"
    # vst_preset_path = "C:/Users/a/dev/midi2wav/presets/labs/drum/labs drums param_drums.FXP"
    # #
    # #
    # # vst_preset_path = "C:/Users/a/dev/midi2wav/presets/ample_guitar/acoustic guitar/ample_guitar guitar param_Export_Audio.FXP"
    # vst_preset_path = "C:/Users/a/dev/midi2wav/presets/echosound_guitar/acoustic_guitar_nylon/echosound_guitar acoustic guitar param_Nylon.vstpreset"
    # # vst_preset_path = "labs/acoustic guitar/labs_acoustic guitar_PeelGuitar1_param1.vstpreset"
    # #
    render(midi_path, output_wav_path, duration, vst, vst_preset_path, use_editor=True)
    # exit()
    #
    # #
    # lib_name = "Chris Hein Guitars DE Library"
    # sub = "4 Steel Guitar/1 Steel_All_In_One/1Steel_Full_DE.nkm"
    # # lib_name = "Chris_Hein_Strings_Compact/Chris Hein - Strings Compact Library"
    # # vst_state_list1 = Path(f"C:/Users/Public/{lib_name}/Instruments").glob("**/*.nki")
    # # vst_state_list2 = Path(f"C:/Users/Public/{lib_name}/Instruments").glob("**/*.nkm")
    # # vst_state_list = list(vst_state_list1) + list(vst_state_list2)
    #
    # # print (len(vst_state_list))
    # # vst_state_list = vst_state_list[:3]
    # curr_inst = "acoustic_guitar_steel"
    #
    # # for i, vst_state in enumerate(vst_state_list) :
    # vst_state = Path(f"C:/Users/Public/{lib_name}/Instruments/{sub}")
    # print (vst_state)
    # print (vst_state.name)
    # output_wav_path = f"C:/Users/a/Downloads/test_{curr_inst}.wav"
    # vst_state_path = f"C:/Users/a/dev/midi2wav/presets/kontakt/{curr_inst}/{vst_state.stem}"

    # print(vst_state_path)
    # render(midi_path, output_wav_path, duration, vst, vst_state_path, use_editor=True)
