"""
For rendering list of tracks with single VST + preset
"""

import numpy as np
import csv
import ast
import random
from collections import defaultdict
from pathlib import Path
from multiprocessing import Pool
from renderer import *


random.seed(20220525)

# VST - VST_PRESET PAIR
# GET INSTRUMENTS
DATA_SPLIT = "train"
VERSION = "v1"
if DATA_SPLIT == "train":
    OUTPUT_DIR = f"F:/midi2wav/lmd_rendered_{DATA_SPLIT}_{VERSION}"
else :
    OUTPUT_DIR = f"lmd_rendered_{DATA_SPLIT}_{VERSION}"

def run(inst, vst_list):

    track_list = prepare_data(inst)
    print ("total track_list:", len(track_list))
    n_tracks_per_vst = len(track_list) // len(vst_list)

    rendered_csv_f = f"lmd_rendered_{DATA_SPLIT}_{VERSION}_{inst}.csv"
    fieldnames = ["lmd_id", "inst_midi_file", "inst_wav_file", "vst_instrument_name", "vst", "vst_preset", "fx_vst", "fx_vst_preset", "duration", "bpm", "estimated_bpm", "genres"]

    with open(rendered_csv_f, "w") as rendered_f:
        writer = csv.DictWriter(rendered_f, fieldnames=fieldnames)
        writer.writeheader()

        for i in range(len(vst_list)) :
            curr_vst_info = vst_list[i]

            vst = curr_vst_info[0]
            vst_preset_path = curr_vst_info[1]
            use_editor = curr_vst_info[2]
            force_editor = curr_vst_info[3]


            if len(curr_vst_info) > 4 :
                fx_vst = curr_vst_info[4]
                fx_vst_preset_path = curr_vst_info[5]
                fx_use_editor = curr_vst_info[6]


            else :
                fx_vst, fx_vst_preset_path, fx_use_editor = None, None, None

            # Get current track list for the current VST
            if i == len(vst_list) -1 :
                curr_track_list = track_list[i * n_tracks_per_vst : ]
            else :
                curr_track_list = track_list[i * n_tracks_per_vst : (i+1) * n_tracks_per_vst]

            print ("curr vst:", vst, fx_vst)
            print ("curr preset", vst_preset_path, fx_vst_preset_path)
            print (len(curr_track_list))

            midi_path_list = []
            output_wav_path_list = []
            duration_list = []
            metadata_list = []

            for inst_midi_file, row in curr_track_list :
                midi_path_list.append(inst_midi_file)

                lmd_id = row["lmd_id"]
                output_wav_dir = Path(OUTPUT_DIR) / lmd_id

                if fx_vst :
                    output_wav_path = output_wav_dir / f"{Path(inst_midi_file).stem}___{Path(vst_preset_path).stem}___{Path(fx_vst_preset_path).stem}.wav"
                else :
                    output_wav_path = ( output_wav_dir
                                        / f"{Path(inst_midi_file).stem}___{Path(vst_preset_path).stem}.wav"
                                    ).as_posix()
                output_wav_path_list.append(output_wav_path)

                duration = round(float(row["duration"]), 1)
                duration_list.append(duration)

                metadata = {
                    "lmd_id": row["lmd_id"],
                    "inst_midi_file": inst_midi_file,
                    "inst_wav_file": output_wav_path,
                    "vst_instrument_name": inst,
                    "vst": vst,
                    "vst_preset": vst_preset_path,
                    "fx_vst" : fx_vst,
                    "fx_vst_preset" : fx_vst_preset_path,
                    "duration": row["duration"],
                    "bpm": row["bpm"],
                    "estimated_bpm": row["estimated_bpm"],
                    "genres": row["genres"],
                }
                metadata_list.append(metadata)

            vst_path = VST_PATH[vst]
            if fx_vst :
                fx_vst_path = VST_PATH[fx_vst]
            else :
                fx_vst_path = None

            render_multiple(
                midi_path_list,
                output_wav_path_list,
                duration_list,
                vst_path,
                vst_preset_path,
                fx_vst_path,
                fx_vst_preset_path,
                use_editor,
                force_editor
            )


            # Write to CSV
            for metadata in metadata_list:

                writer.writerow(metadata)



def prepare_data(inst):
    counter = 0

    parsed_csv_f = f"lmd_parsed_{DATA_SPLIT}_{VERSION}.csv"

    metadata_list, midi_path_list, output_wav_path_list, duration_list = [], [], [], []

    track_list = []

    # Get all tracks with current instrument
    with open(parsed_csv_f) as f:
        reader = csv.DictReader(f)

        for row in reader:

            # duration = round(float(row["duration"]), 1)

            # lmd_id = row["lmd_id"]
            # output_wav_dir = Path(output_dir) / lmd_id
            # print ("output_wav_dir" ,output_wav_dir)

            # Render each instruments in this track
            for vst_inst in list(VST_TO_MIDI_INST.keys()):
                if vst_inst != inst:
                    continue

                if len(row[vst_inst]) > 0 :

                    inst_midi_files = row[vst_inst].split(",")

                    for inst_midi_file in inst_midi_files :
                        track_list.append((inst_midi_file, row))

    # Divide the tracks per vst
    random.shuffle(track_list)

    return track_list


if __name__ == "__main__":
    base_dir = Path("C:/Users/a/dev/midi2wav")


    ##########################  Electric bass  ##########################

    inst = "electric_bass"
    vst_list = [
        (
            "kontakt",
            (
                base_dir
                / "presets/kontakt/electric_bass/kontakt__electric_bass__scarbee_jay_bass_both"
            ).as_posix(),
            True,
            False,
        ),
        (
            "ample_bass",
            (
                base_dir
                / "presets/ample_bass/electric_bass/ample_bass__electric_bass__default.fxp"
            ).as_posix(),
            False,
            False
        ),
        (
            "labs",
            (
                base_dir
                / "presets/labs/electric_bass/labs__electric_bass__classic_bass_amp.fxp"
            ).as_posix(),
            False,
            False
        ),
        (
            "labs",
            (
                base_dir
                / "presets/labs/electric_bass/labs__electric_bass__warm_bass_amp.fxp"
            ).as_posix(),
            False,
            False
        ),
    ]

    run(inst, vst_list)

    ##########################  Contrabass  ##########################

    # inst = "contrabass"
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/contrabass/kontakt__contrabass__CB1_Solo_Contrabass").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/contrabass/kontakt__contrabass__CB2_German_Bass").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/contrabass/kontakt__contrabass__CB3_Italian_Bass").as_posix(),
    #     True,
    #     False
    #     ),
    # ]
    # run(inst, vst_list)

    # ##########################  Slap bass  ##########################

    inst = "slap_bass"
    vst_list = [
        (
            "kontakt",
            (base_dir / "presets/kontakt/slap_bass/kontakt__slap_bass__scarbee_jay_bass_slap_both").as_posix(),
            True,
            False
        )
    ]
    run(inst, vst_list)


    # ######################### Distortion guitar  ##########################
    inst = "distortion_guitar"
    preset_list = [
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__alternative_crunch",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__cheesy_80s_metal_lead",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__straightforward_metal",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__driven_delay",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__double_screamer",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__horrible_punk_tone",
    "presets/guitar_rig/distortion_guitar/guitar_rig__distortion_guitar__massive_wall_of_rock",
    ]

    vst_list = []
    for preset in preset_list :
        vst_list.append(
            (
            "realstrat5",
            (base_dir / "presets/realstrat5/distortion_guitar/realstrat5__distortion_guitar__default").as_posix(),
            True,
            False,
            "guitar_rig",
            (base_dir / preset).as_posix(),
            True
            )
        )

    run(inst, vst_list)

    # ######################### Electric guitar clean  ##########################

    inst = "electric_guitar_clean"
    preset_list = [
        "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__clean_break",
        "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__funky_autowah",
        "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__greasy_lightning",
        "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__simple_guitar_tone",
        "presets/guitar_rig/electric_guitar_clean/guitar_rig__electric_guitar_clean__use_your_dynamics"
    ]
    vst_list = []
    for preset in preset_list :
        vst_list.append(
            (
            "realstrat5",
            (base_dir / "presets/realstrat5/electric_guitar_clean/realstrat5__electric_guitar_clean__default").as_posix(),
            True,
            False,
            "guitar_rig",
            (base_dir/ preset).as_posix(),
            True
            )
        )

    vst_list.append(
        (
        "kontakt",
        (base_dir / "presets/kontakt/electric_guitar_clean/kontakt__electric_guitar_clean__1EG_Clean_Full_M_DE").as_posix(),
        True,
        False
        )
    )

    run(inst, vst_list)

    # ######################### Acoustic guitar steel  ##########################
    inst = "acoustic_guitar_steel"
    vst_list = [
        (
         "ample_guitar",
         (base_dir / 'presets/ample_guitar/acoustic_guitar_steel/ample_guitar__acoustic_guitar_steel__default.fxp' ).as_posix(),
         False,
         False
        ),
        (
        "echosound_guitar",
        (base_dir / "presets/echosound_guitar/acoustic_guitar_steel/echosound_guitar__acoustic_guitar_steel__default.vstpreset").as_posix(),
        False,
        False
        ),
        (
        "kontakt",
        (base_dir / "presets/kontakt/acoustic_guitar_steel/kontakt__acoustic_guitar_steel__1Steel_Full_M_DE").as_posix(),
        True,
        False
        )
    ]

    run(inst, vst_list)

    ########################## Acoustic guitar nylon  ##########################
    # inst = "acoustic_guitar_nylon"
    # vst_list = [
    #     (
    #         "echosound_guitar",
    #         (
    #             base_dir
    #             / "presets/echosound_guitar/acoustic_guitar_nylon/echosound_guitar__acoustic_guitar_nylon__default.vstpreset"
    #         ).as_posix(),
    #         False,
    #         False
    #     ),
    #     (
    #      "ample_guitar",
    #      (base_dir / 'presets/ample_guitar/acoustic_guitar_nylon/ample_guitar__acoustic_guitar_steel__default.fxp' ).as_posix(),
    #      False,
    #      False
    #     ),
    #
    # ]
    # run(inst, vst_list)

    # ######################### Choir  ##########################
    # inst = "choir"
    # preset_list = [
    #     "presets/labs/choir/labs__choir__michahs_choir_the_choir.fxp",
    #     "presets/labs/choir/labs__choir__michahs_choir_sustain_ahhs.fxp",
    #     "presets/labs/choir/labs__choir__michahs_choir_sustain_oohs.fxp",
    #     "presets/labs/choir/labs__choir__michahs_choir_sustain_uhs.fxp",
    #     "presets/labs/choir/labs__choir__michahs_choir_warp_laments.fxp",
    #     "presets/labs/choir/labs__choir__michahs_choir_warp_souls.fxp"
    # ]
    # vst_list = []
    # for preset in preset_list :
    #     vst_list.append((
    #         "labs",
    #         (base_dir / preset).as_posix(),
    #         False,
    #         False
    #     ))
    #
    # run(inst, vst_list)
    # ######################### Drum ##########################
    # inst = "drum"
    # vst_list = [
    #     (
    #     "addictive_drums",
    #     (base_dir / "presets/addictive_drums/drum/addictive_drums__drum__ad2_dryroom").as_posix(),
    #     True,
    #     True
    #     ),
    #     (
    #     "mt_powerdrumkit",
    #     (base_dir / "presets/mt_powerdrumkit/drum/mt_powerdrumkit__drum__default").as_posix(),
    #     True,
    #     True
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/drum/labs__drum__drums.fxp").as_posix(),
    #     False,
    #     True
    #     )
    # ]
    #
    # run(inst, vst_list)

    ######################### Acoustic piano ##########################
    inst = "acoustic_piano"
    preset_list = [
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_studio_grand",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_mono_tube",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_mr_bright",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_aged_strings",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_roomy_pop",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_diy_harpsichord",
        "presets/addictive_keys/acoustic_piano/addictive_keys__acoustic_piano__studio_grand_tight_studio"
    ]
    vst_list = []
    for preset in preset_list :
        vst_list.append((
            "addictive_keys",
            (base_dir / preset).as_posix(),
            True,
            False
        ))

    run(inst, vst_list)


    # ########################## Electric piano ##########################
    inst = "electric_piano"
    preset_list = [
     "presets/labs/electric_piano/labs__electric_piano__electric_piano_di.fxp",
     "presets/labs/electric_piano/labs__electric_piano__electric_piano_chorus.fxp",
     "presets/labs/electric_piano/labs__electric_piano__soft_piano_soft_piano.fxp",
     "presets/labs/electric_piano/labs__electric_piano__rare_groove_piano_sustain.fxp",
     "presets/labs/electric_piano/labs__electric_piano__wurli_di.fxp",
     "presets/labs/electric_piano/labs__electric_piano__glass_piano_anthem.fxp",
     "presets/labs/electric_piano/labs__electric_piano__glass_piano_glass_grand.fxp"
    ]
    vst_list = []
    for preset in preset_list :
        vst_list.append((
            "labs",
            (base_dir / preset).as_posix(),
            False,
            False
        ))

    run(inst, vst_list)

    #
    # ######################### Strings ##########################

    # inst = "strings"
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/strings/kontakt__strings__EnsString_All_Mix_Full_High").as_posix(),
    #     True,
    #     True
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/strings/labs__strings__strings_ensemble.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/strings/labs__strings__strings_long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/strings/labs__strings__expressive_strings.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/strings/bbc_symphony__violin__violin1_long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/strings/bbc_symphony__viola__violoas_long.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # ######################### Violin ##########################
    # inst = "violin"
    #
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/violin/kontakt__violin__VLN1_Solo_Violin").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/violin/bbc_symphony__violin__violin1_long.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # # ######################### Viola ##########################
    # inst = "viola"
    #
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/viola/kontakt__viola__VLA1_Solo_Viola").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/viola/bbc_symphony__viola__violoas_long.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # # ######################### Cello ##########################
    # inst = "cello"
    #
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/cello/kontakt__cello__VLC2_Modern_Cello").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/cello/bbc_symphony__cello__cellis_long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/cello/labs__cello__amplified_cello_quartet_tension.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/cello/labs__cello__cello_moods_c_awe.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/cello/labs__cello__cello_moods_c_melancholy.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    # ]
    # run(inst, vst_list)

    ######################### Flute ##########################
    # inst = "flute"
    #
    # vst_list = [
    #     (
    #     "iowa_flute",
    #     (base_dir / "presets/iowa_flute/flute/iowa_flute__flute__default.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/flute/bbc_symphony__flute__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/flute/kontakt__flute__C_Flute1_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    # #
    # # ######################### Clarinet ##########################
    # inst = "clarinet"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/clarinet/bbc_symphony__clarinet__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/clarinet/kontakt__clarinet__Bb_Clarinet_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    # #
    # # ######################### Piccolo ##########################
    # inst = "piccolo"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/piccolo/bbc_symphony__piccolo__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/piccolo/kontakt__piccolo__Flute_Piccolo_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    # #
    # ######################### English horn ##########################
    # inst = "english_horn"
    #
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/english_horn/kontakt__english_horn__English_Horn_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    # #
    # # ######################### Tuba ##########################
    # inst = "tuba"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/tuba/bbc_symphony__tuba__long.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)

    # ######################### Organ ##########################
    # inst = "organ"
    #
    # vst_list = [
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/organ/labs__organ__pipe_organ_full_organ.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "collab3",
    #     (base_dir / "presets/collab3/organ/collab3__organ__default.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)

    # ######################### French horn ##########################
    # inst = "french_horn"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/french_horn/bbc_symphony__french_horn__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/french_horn/kontakt__french_horn__French_horn_1_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # ######################### Trumpet ##########################
    # inst = "trumpet"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/trumpet/bbc_symphony__trumpet__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/trumpet/kontakt__trumpet__Trumpet_1_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    #
    # preset_list = [
    #     "presets/labs/trumpet/labs__trumpet__trumpet_fields_dirty_vibrato.fxp",
    #     "presets/labs/trumpet/labs__trumpet__trumpet_fields_floating_vibrato.fxp",
    #     "presets/labs/trumpet/labs__trumpet__trumpet_fields_nice_and_steady1.fxp",
    #     "presets/labs/trumpet/labs__trumpet__trumpet_fields_nice_and_steady2.fxp",
    #     "presets/labs/trumpet/labs__trumpet__trumpet_fields_slow_bending.fxp"
    # ]
    # for preset in preset_list :
    #     vst_list.append(
    #         (
    #         "labs",
    #         (base_dir / preset).as_posix(),
    #         False,
    #         False
    #         )
    #     )
    #
    # run(inst, vst_list)
    #
    # ######################### Trombone ##########################
    # inst = "trombone"
    #
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/trombone/bbc_symphony__trombone__tenor_long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/trombone/kontakt__trombone__Trombone_1_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # ######################### Brass ##########################
    # inst = "brass"
    #
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/brass/kontakt__brass__French_Horn_Ensemble_Compact").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/brass/kontakt__brass__Trombone_Ensemble_Compact").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/brass/kontakt__brass__Trumpet_Ensemble_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # ########################## Oboe ##########################
    #
    # inst = "oboe"
    # vst_list = [
    #     (
    #     "kontakt",
    #     (base_dir /"presets/kontakt/oboe/kontakt__oboe__Oboe1_compact").as_posix(),
    #     True,
    #     False
    #     ),
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/oboe/bbc_symphony__oboe__long.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    # #
    # # ########################## Harp ##########################
    # #
    # inst = "harp"
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/harp/bbc_symphony__harp__plucks.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "labs",
    #     (base_dir / "presets/labs/harp/labs__harp__autoharp_plucked.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # ########################## Bassoon ##########################
    # inst = "bassoon"
    # vst_list = [
    #     (
    #     "bbc_symphony",
    #     (base_dir / "presets/bbc_symphony/bassoon/bbc_symphony__bassoon__long.fxp").as_posix(),
    #     False,
    #     False
    #     ),
    #     (
    #     "kontakt",
    #     (base_dir / "presets/kontakt/bassoon/kontakt__bassoon__Bassoon1_Compact").as_posix(),
    #     True,
    #     False
    #     )
    # ]
    # run(inst, vst_list)

    ######################### Electric guitar jazz ##########################
    # inst = "electric_guitar_jazz"
    #
    # preset_list = [
    #     "presets/guitar_rig/electric_guitar_jazz/guitar_rig__electric_guitar_clean__clean_break",
    #     "presets/guitar_rig/electric_guitar_jazz/guitar_rig__electric_guitar_clean__greasy_lightning",
    #     "presets/guitar_rig/electric_guitar_jazz/guitar_rig__electric_guitar_clean__simple_guitar_tone",
    #     "presets/guitar_rig/electric_guitar_jazz/guitar_rig__electric_guitar_clean__use_your_dynamics"
    # ]
    # vst_list = []
    # for preset in preset_list :
    #     vst_list.append(
    #         (
    #         "realstrat5",
    #         (base_dir / "presets/realstrat5/electric_guitar_jazz/realstrat5__electric_guitar_clean__default").as_posix(),
    #         True,
    #         False,
    #         "guitar_rig",
    #         (base_dir/ preset).as_posix(),
    #         True
    #         )
    #     )
    #
    # vst_list.append((
    #     "kontakt",
    #     (base_dir / "presets/kontakt/electric_guitar_jazz/kontakt__electric_guitar_jazz__1Jazz_Fi_Full").as_posix(),
    #     True,
    #     False
    # ))
    # run(inst, vst_list)
    #
    # # # ##########################  Synth bass  ##########################
    # #
    # inst = "synth_bass"
    # vst_list = [
    #     (
    #         "kontakt",
    #         (
    #             base_dir
    #             / "presets/kontakt/synth_bass/kontakt__electric_bass__scarbee_jay_bass_both"
    #         ).as_posix(),
    #         True,
    #         False
    #     ),
    #     (
    #         "ample_bass",
    #         (
    #             base_dir
    #             / "presets/ample_bass/synth_bass/ample_bass__electric_bass__default.fxp"
    #         ).as_posix(),
    #         False,
    #         False
    #     ),
    #     (
    #         "labs",
    #         (
    #             base_dir
    #             / "presets/labs/synth_bass/labs__electric_bass__classic_bass_amp.fxp"
    #         ).as_posix(),
    #         False,
    #         False
    #     ),
    #     (
    #         "labs",
    #         (
    #             base_dir
    #             / "presets/labs/synth_bass/labs__electric_bass__warm_bass_amp.fxp"
    #         ).as_posix(),
    #         False,
    #         False
    #     ),
    # ]
    # run(inst, vst_list)
    #
    # # # ##########################  sax  ##########################
    # inst = "alto_sax"
    # vst_list = [
    #     (
    #     "iowa_alto_sax",
    #     (base_dir / "presets/iowa_alto_sax/alto_sax/iowa_alto_sax__saxophone__default.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # inst = "soprano_sax"
    # vst_list = [
    #     (
    #     "iowa_alto_sax",
    #     (base_dir / "presets/iowa_alto_sax/soprano_sax/iowa_alto_sax__saxophone__default.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # inst = "tenor_sax"
    # vst_list = [
    #     (
    #     "iowa_alto_sax",
    #     (base_dir / "presets/iowa_alto_sax/tenor_sax/iowa_alto_sax__saxophone__default.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
    #
    # inst = "baritone_sax"
    # vst_list = [
    #     (
    #     "iowa_alto_sax",
    #     (base_dir / "presets/iowa_alto_sax/baritone_sax/iowa_alto_sax__saxophone__default.fxp").as_posix(),
    #     False,
    #     False
    #     )
    # ]
    # run(inst, vst_list)
