import librosa
import numpy as np

def load_audio(
    path, mono: bool, target_sr=None, offset=0.0, duration=None
) -> np.ndarray:
    """
    Return:
        y (np.ndarray): channel-first 2D audio signal

    """
    y, orig_sr = librosa.load(
        path, sr=None, mono=mono, offset=offset, duration=duration
    )  # (N,) if mono, (2, N) if stereo
    if target_sr is not None:
        y = resample(y, orig_sr=orig_sr, target_sr=target_sr)
    return np.atleast_2d(y)


def resample(signal: np.ndarray, orig_sr: int, target_sr: int, **kwargs) -> np.ndarray:
    """signal: (N,) or (num_channel, N)"""
    return librosa.resample(
        y=signal, orig_sr=orig_sr, target_sr=target_sr, res_type='polyphase', **kwargs
    )
