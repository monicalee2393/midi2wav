from pathlib import Path
from collections import defaultdict
import json
import csv
import pretty_midi
from config import *

MIN_NOTE_COUNT = 100


# LMD ID to MSD ID mapping
lmd_to_msd_f = "labels/matched_ids.txt"
lmd_to_msd = {}

with open(lmd_to_msd_f) as f:
    lines = f.readlines()
    for line in lines:
        lmd_id, msd_id = line.split("    ")
        lmd_to_msd[lmd_id.strip()] = msd_id.strip()

msd_to_lmd = {v: k for k, v in lmd_to_msd.items()}

print("msd matched", len(list(msd_to_lmd.keys())))

# MIDI tracks per genre mapping. ex. "rock" : [track1, track2, ...]
midi_per_genre = defaultdict(list)

# MIDI genre labels
lastfm = list(Path("labels/lastfm").glob("*.txt"))
tagtraum = list(Path("labels/tagtraum").glob("*.txt"))
amg = list(Path("labels/amg").glob("*.txt"))

genre_label_files = lastfm + tagtraum + amg

# Metadata info for MIDI files
midi_info = json.load(open("labels/midi_info.json"))

# Find all tracks for each genre
for genre_f in genre_label_files:
    genre = genre_f.stem.split("_")[-1]
    genre = genre.lower()
    if "favo" in genre:
        genre = "favorites"

    # if genre in ["chill", "relax", "chillout"]:
    #     genre = "chill"

    # if genre in ["melancholy", "sad", "downtempo", "mellow"]:
    #     genre = "sad"

    # if genre == "rap" :
    #     genre = "hip-hop"

    # if genre in ["60s", "70s", "80s", "90s", "00s"] :
    #     genre = "oldies"

    # if genre in ["new-age", "blues"]:
    #     genre = "jazz"

    # if genre in ["world", "latin", "reggae"] :
    #     genre = "world"

    # if genre in ["house", "electro", "techno", "electronica", "trance"]:
    #     genre = "electronic"

    # if genre in ["soul", "rnb"]:
    #     genre = "rnb"

    # if genre in ["alternative", "indie", "punk"]:
    #     genre = "rock"

    with open(genre_f, "r") as f:
        lines = f.readlines()
        for line in lines:
            msd_id = line.strip()
            # print (msd_id)
            # print (msd_id)
            # if msd_id in msd_to_lmd :
            #     midi_per_genre[genre_f.name.split(".")[0]].append(msd_to_lmd[msd_id])
            if msd_id in msd_to_lmd:
                lmd_id = msd_to_lmd[msd_id]
                midi_per_genre[genre].append(lmd_id)

all_tracks = []
tracks_and_its_genres = defaultdict(list)

for k, v in midi_per_genre.items():
    set_v = list(set(v))

    all_tracks.extend(set_v)

    for lmd_id in set_v:
        tracks_and_its_genres[lmd_id].append(k)

# Save MIDI and its genre labels and instrument composition
with open("lmd_filtered.csv", "w") as f:
    csvwriter = csv.DictWriter(
        f,
        fieldnames=["lmd_id", "msd_id", "bpm", "estimated_bpm", "genres", "duration"]
        + INSTRUMENT_NAMES,
    )
    csvwriter.writeheader()

    counter = 0
    for i, (k, v) in enumerate(tracks_and_its_genres.items()):
        if counter % 100 == 0:
            print(counter, len(list(tracks_and_its_genres.keys())))

        # if counter > 100 :
        #     break
        counter += 1

        data = {"lmd_id": k, "msd_id": lmd_to_msd[k]}

        # Load MIDI
        midi_f = Path(ORIG_MIDI_DIR) / k[0] / f"{k}.mid"

        try:
            curr_midi = pretty_midi.PrettyMIDI(midi_f.as_posix())
        except:
            continue

        # Get current instruments
        curr_instruments = []
        for inst in curr_midi.instruments:

            if len(inst.notes) < MIN_NOTE_COUNT:
                continue
            if inst.is_drum:
                inst_name = "drum"
            else:
                midi_program_name = pretty_midi.program_to_instrument_name(inst.program)
                if midi_program_name in MIDI_PROGRAM_NAME_TO_INSTRUMENT_NAME.keys():
                    inst_name = MIDI_PROGRAM_NAME_TO_INSTRUMENT_NAME[midi_program_name]
                else:
                    inst_name = "others"
            data[inst_name] = 1
            curr_instruments.append(inst_name)
        curr_instruments = list(set(curr_instruments))

        # Missing inst
        missing = list(set(INSTRUMENT_NAMES) - set(curr_instruments))
        for inst in missing:
            data[inst] = 0

        # Get BPM info
        if midi_info[k]["constant_tempo"]:
            bpm = int(midi_info[k]["constant_tempo"])
        else:
            bpm = -1

        # Get estimated BPM
        try:
            estimated_tempo = curr_midi.estimate_tempo()
        except:
            continue

        # Get duration
        duration_sec = curr_midi.get_end_time()

        data["bpm"] = bpm
        data["estimated_bpm"] = estimated_tempo
        data["genres"] = ",".join(v)
        data["duration"] = duration_sec
        csvwriter.writerow(data)
