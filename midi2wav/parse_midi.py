"""
Parse multitrack midi into single inst track midi files

Instrument class and instrument name list = https://github.com/craffel/pretty-midi/blob/5e3db4bfa6be0d6e87d7a9e8fcf5f4ed81e97a8d/pretty_midi/constants.py#L7
"""
import os
import pretty_midi
from collections import defaultdict
from pathlib import Path
import csv
from config import *

MIN_NOTE_COUNT = 10


def parse_midi(midi_path, output_dir):
    """multitrack midi -> single track midis

    midi file name convention : trackName__instName_instCounter.mid
        - instName : pretty_midi instrument name
        - instCounter : Nth number of the same instrument in the track

    Params :
        midi_path : path to multitrack midi
        output_dir : directory to save all the single instrument midi files
    Returns :
        inst_midi_list : dictionary with "inst_class" as key and list_of_midi_tracks as value
                         ex. {"guitar": "songname__guitar_electric guitar.mid"}
    """
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    midi_data = pretty_midi.PrettyMIDI(midi_path)
    midi_name = midi_path.split("/")[-1].replace(".mid", "")
    print(midi_name)

    inst_midi_map = defaultdict(list)
    inst_counter = defaultdict(int)

    for instrument in midi_data.instruments:
        program_num = instrument.program
        if instrument.is_drum:
            midi_program_name = "Drum"
        else:
            midi_program_name = pretty_midi.program_to_instrument_name(program_num)

        if len(instrument.notes) > MIN_NOTE_COUNT:
            inst_mid = midi_data  # set it as the same object as the original
            inst_mid.instruments = [instrument]

            # check if there is the same instrument already present
            curr_counter = inst_counter[midi_program_name]

            inst_mid_output_name = (
                f"{midi_name}__{midi_program_name}_{curr_counter}.mid"
            )
            print(inst_mid_output_name)
            inst_mid.write((Path(output_dir) / inst_mid_output_name).as_posix())

            if midi_program_name in MIDI_INST_TO_VST.keys():
                inst_name = MIDI_INST_TO_VST[midi_program_name]
            else:
                inst_name = "others"

            inst_midi_map[inst_name].append(
                (Path(output_dir) / inst_mid_output_name).as_posix()
            )
            inst_counter[midi_program_name] += 1

    print("Success")
    return inst_midi_map


def process():

    # CSV with lakhmidi filtered list
    filtered_csv_f = "lmd_filtered.csv"
    parsed_csv_f = "lmd_parsed.csv"

    output_dir = Path("lmd_parsed")

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    counter = 0

    with open(filtered_csv_f, "r") as filtered_f, open(parsed_csv_f, "w") as parsed_f:
        reader = csv.DictReader(filtered_f)

        new_fieldnames = [
            "lmd_id",
            "msd_id",
            "bpm",
            "estimated_bpm",
            "genres",
            "duration",
            "others",
        ] + list(VST_TO_MIDI_INST.keys())

        writer = csv.DictWriter(parsed_f, fieldnames=new_fieldnames)
        writer.writeheader()

        for row in reader:
            # if counter > 50:
            #     break

            midi_output_subdir = row["lmd_id"]  # == track name

            midi_path = Path(ORIG_MIDI_DIR) / row["lmd_id"][0] / f"{row['lmd_id']}.mid"

            inst_class_midi_map = parse_midi(
                midi_path.as_posix(), (output_dir / midi_output_subdir).as_posix()
            )
            row = {
                "lmd_id": row["lmd_id"],
                "msd_id": row["msd_id"],
                "bpm": row["bpm"],
                "estimated_bpm": row["estimated_bpm"],
                "genres": row["genres"],
                "duration": row["duration"],
            }

            for k, v in inst_class_midi_map.items():
                print(k, v)
                row.update({k: ",".join(v)})

            writer.writerow(row)

            counter += 1


if __name__ == "__main__":

    process()
