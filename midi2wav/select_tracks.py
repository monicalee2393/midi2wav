"""
Select N tracks to render.

Select from lmd_parsed.csv and save to lmd_parsed_{date}.csv
"""
import csv
import random
from collections import defaultdict
from config import *

random.seed(20220525)

GENRE_LIST = ["pop", "rock", "jazz", "electronic", "metal", "country", "rnb", "hip-hop", "latin", "blues"]
MIN_TRACKS_PER_INST = 250
MIN_TRACKS_PER_GENRE = 250
# N_TRAIN = 1000

MIN_TRACKS_PER_INST_VALID = 50
MIN_TRACKS_PER_GENRE_VALID = 50
# N_VALID = 200

VERSION = "v1"

parsed_csv_f = "lmd_parsed.csv"
selected_train_csv_f = f"lmd_parsed_train_{VERSION}.csv"
selected_valid_csv_f = f"lmd_parsed_valid_{VERSION}.csv"

COMMON_INST_LIST = ["acoustic_piano", "electric_piano", "electric_bass", "synth_bass", "slap_bass", "electric_guitar_clean", "distortion_guitar", "drum", "acoustic_guitar_steel", "acoustic_guitar_nylon"]
RARE_INST_LIST = list(set(VST_TO_MIDI_INST.keys()) - set(COMMON_INST_LIST))
print (RARE_INST_LIST)

all_tracks = []

cnt = 0
with open(parsed_csv_f, "r") as f:
    reader = csv.DictReader(f)
    header = None

    for row in reader:
        if cnt == 0 :
            header = row
        else :
            all_tracks.append(row)
        cnt += 1

random.shuffle(all_tracks)
print ("ALL TRACKS", len(all_tracks))


def make_data(track_list, min_tracks_per_genre, min_tracks_per_inst):

    ### 장르별로 트랙 골고루 가져오기
    genre_counter = defaultdict(list)

    for row in track_list :
        genres = row["genres"].split(",")
        for g in GENRE_LIST :
            if g in genres :
                genre_counter[g].append(row["lmd_id"])

    selected_genre_tracks_id = []
    for k,v in genre_counter.items():
        selected_genre_tracks_id.extend(v[:int(min_tracks_per_genre * 1.5)])


    selected_genre_tracks_id = list(set(selected_genre_tracks_id))
    print ("N tracks", len(selected_genre_tracks_id))

    selected_genre_tracks = []
    for row in all_tracks :
        if row["lmd_id"] in selected_genre_tracks_id :
            selected_genre_tracks.append(row)

    random.shuffle(selected_genre_tracks)


    #### 각 악기가 최소한 N개 존재하도록 트랙 가져오기

    specific_tracks = []
    other_tracks = []
    inst_counter = { k : 0 for k in list(MSG_INST_TO_VST.keys()) }
    genre_counter = { k : 0 for k in GENRE_LIST}
    print (inst_counter)


    for row in selected_genre_tracks :
        # 모든 악기가 최소 트랙 갯수 만큼 있으면 멈추기
        lowest = min(inst_counter.items(), key=lambda x: x[1])

        if lowest[1] > min_tracks_per_inst :
            break

        is_appended = False

        genres = row["genres"].split(",")

        for rare_inst in RARE_INST_LIST :
            if len(row[rare_inst]) > 0 :
                if not is_appended :

                    for k,v in genre_counter.items():
                        if k in genres:
                            genre_counter[k] += 1

                    specific_tracks.append(row)
                    is_appended = True

                inst_counter[VST_TO_MSG_INST[rare_inst]] += 1


        for common_inst in COMMON_INST_LIST :
            if len(row[common_inst]) > 0:
                inst_counter[VST_TO_MSG_INST[common_inst]] +=1

        if not is_appended:
            other_tracks.append(row)

    print (len(specific_tracks))
    print (len(other_tracks))


    for k,v in inst_counter.items():
        print (k,v)

    for k,v in genre_counter.items():
        print (k,v)

    return specific_tracks

train_tracks = make_data(all_tracks, MIN_TRACKS_PER_GENRE, MIN_TRACKS_PER_INST)


train_track_ids = []

for t in train_tracks :
    train_track_ids.append(t['lmd_id'])
print ("train tracks", len(train_tracks))


other_tracks = []
for t in all_tracks:

    if t["lmd_id"] not in train_track_ids:
        other_tracks.append(t)


valid_tracks = make_data(other_tracks, MIN_TRACKS_PER_GENRE_VALID, MIN_TRACKS_PER_INST_VALID)

print("valid tracks", len(valid_tracks))



header = list(header.keys())
print (header)


with open(selected_train_csv_f, "w") as f:
    writer = csv.DictWriter(f, fieldnames=header)
    writer.writeheader()

    for track in train_tracks :
        writer.writerow(track)

with open(selected_valid_csv_f, "w") as f:
    writer = csv.DictWriter(f, fieldnames=header)
    writer.writeheader()

    for track in valid_tracks:
        writer.writerow(track)
