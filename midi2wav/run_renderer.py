import numpy as np
import csv
import ast
import random
from collections import defaultdict
from pathlib import Path
from multiprocessing import Pool
from renderer import *

def run():
    metadata_list, data_list = prepare_data()
    failed_list = []
    print (len(metadata_list))
    with Pool(1) as p :
        return_list = p.starmap(render, data_list)

    rendered_csv_f = "lmd_rendered.csv"
    with open(rendered_csv_f, "w") as rendered_f:

        fieldnames = ["lmd_id", "inst_midi_file", "inst_wav_file", "vst_instrument_name", "vst", "vst_preset", "duration", "bpm", "estimated_bpm", "genres"]
        writer = csv.DictWriter(rendered_f, fieldnames=fieldnames)
        writer.writeheader()

        for metadata, data, ret in zip(metadata_list, data_list, return_list) :

            if ret :
                writer.writerow(metadata)
            else :
                failed_list.append(
                    data
                )

    for failed in failed_list :
        print (failed)


def prepare_data():

    counter = 0

    VST_MAP = load_vst_presets()

    parsed_csv_f = "lmd_parsed.csv"

    metadata_list, data_list = [], []

    with open(parsed_csv_f) as f :
        reader = csv.DictReader(f)

        for row in reader:
            if counter > 3 :
                break
            counter += 1

            duration = round(float(row["duration"]), 1)


            lmd_id = row["lmd_id"]
            output_wav_dir = Path(OUTPUT_DIR) / lmd_id
            print ("output_wav_dir" ,output_wav_dir)

            # Render each instruments in this track
            for vst_inst in list(VST_TO_MIDI_INST.keys()):

                if len(row[vst_inst]) > 0:
                    print (row[vst_inst])
                    inst_midi_files = row[vst_inst].split(",")
                    # There can be multiple tracks for the same instrument
                    for inst_midi_file in inst_midi_files:
                        # inst_midi_file : lmd_parsed/lmd_id/inst_track_name..mid
                        print(vst_inst, inst_midi_file)

                        output_wav_path = (output_wav_dir / Path(inst_midi_file).name.replace(".mid", ".wav")).as_posix()

                        # If the vst_inst is not available as VST in our system, skip rendering
                        if vst_inst not in list(VST_MAP.keys()):
                            print (f"{vst_inst} is not available as VST.")
                            continue

                        # Choose vst and its preset randomly
                        vst_options = list(VST_MAP[vst_inst].keys())
                        curr_vst = random.choice(vst_options)

                        curr_preset_list = VST_MAP[vst_inst][curr_vst]["presets"]
                        n_presets = len(curr_preset_list)
                        print (VST_MAP[vst_inst][curr_vst])
                        print (n_presets)

                        # If there are no presets, skip rendering
                        if n_presets == 0 :
                            continue

                        # Choose random preset
                        preset_idx = random.randint(0, n_presets - 1)

                        vst_path_tmp = VST_MAP[vst_inst][curr_vst]["path"].lower()

                        # Hacky way of making some VSTs work in dawdreamer
                        use_editor = False
                        if ("kontakt" in vst_path_tmp) or ("realstrat" in vst_path_tmp) :
                            use_editor = True


                        data_list.append((
                            inst_midi_file,
                            output_wav_path,
                            duration,
                            VST_MAP[vst_inst][curr_vst]["path"],
                            VST_MAP[vst_inst][curr_vst]["presets"][preset_idx],
                            use_editor
                        ))

                        metadata_list.append({
                            "lmd_id" : row["lmd_id"],
                            "inst_midi_file" : inst_midi_file,
                            "inst_wav_file" : output_wav_path,
                            "vst_instrument_name": vst_inst,
                            "vst" : curr_vst,
                            "vst_preset": VST_MAP[vst_inst][curr_vst]["presets"][preset_idx],
                            "duration" : row["duration"],
                            "bpm": row["bpm"],
                            "estimated_bpm": row["estimated_bpm"],
                            "genres": row["genres"]
                        })

    return metadata_list, data_list




if __name__ == "__main__":
    run()
