import random
from argparse import ArgumentParser
from pathlib import Path
from xml.dom.pulldom import parseString
import pretty_midi
import soundfile as sf
import numpy as np
from constants import *
from midi2wav import midi2wav


def test_instrument_preset(inst, vst, preset_file):
    """ Generate with single instrument preset """
    assert inst in list(
        VST_TO_MIDI_INST.keys()
    ), f"{inst} is not an accepted vst instrument. Choose from {list(VST_TO_MIDI_INST.keys())}"

    max_test_files = 1
    counter = 0

    midi_files = (Path("test/midi/by_instruments") / inst).glob("*.mid")
    midi_files = list(midi_files)
    random.shuffle(midi_files)

    for midi_file in midi_files:
        if counter >= max_test_files:
            break
        mid_data = pretty_midi.PrettyMIDI(midi_file.as_posix())
        duration = int(mid_data.get_end_time())

        output_wav_path = (
            Path("test/wav/by_instruments")
            / inst
            / midi_file.name.replace(".mid", ".wav")
        )
        print (midi_file)
        print (vst)
        print (preset_file)
        midi2wav(
            midi_file.as_posix(), output_wav_path.as_posix(), duration, vst, preset_file
        )
        print(f"Generated...{output_wav_path}")
        counter += 1


def test_all_instruments():
    """ Generate all instruments in test tracks with random vst + presets  """
    midi_tracks = (Path("test/midi/by_tracks")).glob("9*")
    # midi_tracks = list(midi_tracks)

    for midi_track in midi_tracks :
        print (midi_track)
        midi_files = Path(midi_track).glob("*.mid")

        output_wav_dir = Path("test/wav/by_tracks") / midi_track.name

        output_wav_files = [] 
        # output_wav_files = output_wav_dir.glob("*.wav")

        for midi_file in midi_files:
            print (midi_file)
            midi_filename = midi_file.name
            midi_inst = midi_filename.split(".mid")[0].split("__")[-1].split("_")[0]

            if not midi_inst in MIDI_INST_TO_VST.keys() : 
                continue 

            vst_inst = MIDI_INST_TO_VST[midi_inst]

            # Get vst and presets for this instrument 
            vst_list = INST_VST_LIST[vst_inst]
            if len(vst_list) == 0 :
                continue 

            # Choose a random vst 
            curr_vst = random.choice(vst_list)
            curr_vst_path = VST_PATH[curr_vst]
            
            print (midi_inst, vst_inst)
            # Choose a random preset for this vst 
            preset_list = [] 
            for ext in ["FXP", "fxp", "vstpreset"] : 
                print ( Path(PRESET_DIR) / curr_vst / vst_inst )
                preset_list.extend(list((Path(PRESET_DIR) / curr_vst / vst_inst).glob(f"*.{ext}"))) 
            
            print (preset_list)
            if len(preset_list) == 0 :
                continue 

            curr_preset = random.choice(preset_list).as_posix()

            print (curr_vst_path)
            print (curr_preset)

            # Render midi2wav 
            output_wav_path = output_wav_dir / midi_filename.replace(".mid", ".wav")
            output_wav_files.append(output_wav_path)

            mid_data = pretty_midi.PrettyMIDI(midi_file.as_posix())
            duration = int(mid_data.get_end_time())

            midi2wav(
                midi_file.as_posix(), output_wav_path.as_posix(), duration, curr_vst_path, curr_preset
            )

        # Combine all instruments into one wav file
        # print (output_wav_files)
        # total_audio_list = []
        # max_len = 0 
        # for output_wav_file in output_wav_files :
        #     data, samplerate = sf.read(output_wav_file.as_posix())
        #     print (data.shape)
        #     if data.shape[0] > max_len :
        #         max_len = data.shape[0]
            
        #     data = np.average(data, axis=1)

        #     total_audio_list.append(data)

        # total_audio = np.zeros((max_len,))
        # for audio in total_audio_list :
        #     padded = np.zeros((max_len,))
        #     padded[: audio.shape[0]] = audio
        #     total_audio += padded * (1 / len(total_audio_list))

        # sf.write((output_wav_dir / "all.wav").as_posix(), total_audio, samplerate)

        print ("DONE")



def test_all_vsts():
    """ Check if dawdreamer can render with vsts """
    
    for vst in VST_PATH.keys() :
        for vst_inst_path in (Path("presets") / vst).glob("*") : 
            if vst_inst_path.name in INST_VST_LIST.keys() :
                print (vst_inst_path)

                vst_inst = vst_inst_path.name
                preset_list = [] 
                for ext in ["FXP", "fxp", "vstpreset"] : 
                    preset_list.extend(list((Path(PRESET_DIR) / vst / vst_inst).glob(f"*.{ext}"))) 

                if len(preset_list) == 0 :
                    continue 

                curr_preset = random.choice(preset_list).as_posix()
                
                # Random track
                midi_tracks = list((Path("test/midi/by_instruments") / vst_inst).glob("*.mid"))
                if len(midi_tracks) == 0 :
                    continue 

                curr_track = random.choice(midi_tracks).as_posix()

                mid_data = pretty_midi.PrettyMIDI(curr_track)
                duration = int(mid_data.get_end_time())
                
                midi2wav(
                    curr_track, curr_track.replace("midi", "wav").replace(".mid", f"_{vst}.wav"), duration, VST_PATH[vst], curr_preset
                )


if __name__ == "__main__":

    # parser = ArgumentParser()
    # parser.add_argument("--instrument", type=str, required=True)
    # parser.add_argument("--vst", type=str, required=True)
    # parser.add_argument("--preset_file", type=str, required=True)

    # args = parser.parse_args()

    # for midi in (Path(PERSONAL_PATH) / PARSED_MIDI_DIR).glob("**/*.mid"):
    #     midi_program_name = midi.as_posix().split("__")[-1].replace(".mid", "").split("_")[0]
    #     print (midi_program_name)
    #     if midi_program_name in MIDI_INST_TO_VST :
    #         vst_inst_name = MIDI_INST_TO_VST[midi_program_name]
    #         copy_midi = midi.name
    #         print (copy_midi)
    #         shutil.copyfile(midi.as_posix(), "test/midi/by_instruments/{}/{}".format(vst_inst_name, copy_midi))




    instrument = "electric piano"
    vst = "/Library/Audio/Plug-Ins/VST/LABS.vst"
    # preset_file = "/Users/bijarimrec/dev/midi2wav/presets/truepianos/acoustic piano/truepianos_acoustic piano_param_atlantis_smooth.FXP"
    # preset_file = "/Users/bijarimrec/dev/midi2wav/presets/LABS/electric piano/labs piano param_piano pads water organ.vstpreset"
    # preset_file = "/Users/bijarimrec/dev/midi2wav/presets/echosound_guitar/acoustic guitar/echosound_guitar acoustic guitar param_Nylon.vstpreset"
    # preset_file = "/Users/bijarimrec/Downloads/labs_drum_default.FXP"
    preset_file = "/Users/bijarimrec/Downloads/labs_electric_piano_glass_piano_anthem.FXP"
    # preset_file = "/Users/bijarimrec/Downloads/labs_electric_piano_glass_piano_glass_grand.vstpreset"
    test_instrument_preset(instrument, vst, preset_file)


    # test_instrument_preset(args.instrument, args.vst, args.preset_file)
    # test_all_instruments()

    # test_all_vsts()